import FileSystem from 'fs'


export default filePath => {

	try {
		var stats = FileSystem.lstatSync(filePath)
	}

	catch (error) {

		if (error.code === 'ENOENT') return false

		throw error

	}

	return stats.isDirectory()

}
